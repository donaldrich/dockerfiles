---
description: Json server
path: tree/master
source: utility/json-server/Dockerfile

---

# json-server

--8<--
description/json-server.md
--8<--

## Reference

* [:octicons-mark-github-16: GitHub](https://github.com/typicode/json-server)

https://devopsheaven.com/api/rest/json/nodejs/docker/2017/09/27/api-rest-json-server-nodejs-docker.html

## Usage

```sh
docker pull donaldrich/function:json-server
docker run -it --rm donaldrich/function:json-server
```

## Configuration

```
--8<--
config/json-server.md
--8<--
```

## Info

??? info ""

    === "GitHub"
        ```json
        --8<--
        github-info/json-server.md
        --8<--
        ```

    === "Image"
        ```json
        --8<--
        image-info/json-server.md
        --8<--
        ```
